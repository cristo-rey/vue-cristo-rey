import Index from './../views/doctores/Paginado.vue';
import Form from './../views/doctores/Form.vue';
const routes = [
	{
		path: '/Doctores',
		name: 'doctores',
		component: Index,
		meta: {
			name: 'Catálogo de Doctores'
		}
	},
	{
		path: '/Doctores/nuevo',
		name: 'doctores.create',
		component: Form,
		meta: {
			name: 'Nuevo Doctor'
		}
	},
	{
		path: '/Doctores/editar/:id',
		name: 'doctores.edit',
		component: Form,
		meta: {
			name: 'Editando Doctor'
		}
	}
];
export default routes;
