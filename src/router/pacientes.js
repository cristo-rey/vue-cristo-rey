import Index from './../views/pacientes/Paginado.vue';
import Form from './../views/pacientes/Form.vue';
import Historial from './../views/pacientes/Historial.vue';
import Orden from './../views/pacientes/Orden.vue';
const routes = [
	{
		path: '/Pacientes',
		name: 'pacientes',
		component: Index,
		meta: {
			name: 'Catálogo de Pacientes'
		}
	},
	{
		path: '/Pacientes/nuevo',
		name: 'pacientes.create',
		component: Form,
		meta: {
			name: 'Nuevo Paciente'
		}
	},
	{
		path: '/Pacientes/editar/:id',
		name: 'pacientes.edit',
		component: Form,
		meta: {
			name: 'Editando Paciente'
		}
	},
	{
		path: '/Pacientes/historial/:id',
		name: 'pacientes.historial',
		component: Historial,
		meta: {
			name: 'Historial Paciente'
		}
	},
	{
		path: '/Pacientes/Nueva-Orden/:id',
		name: 'pacientes.nuevaOrden',
		component: Orden,
		meta: {
			name: 'Nueva Orden'
		}
	}
];
export default routes;
