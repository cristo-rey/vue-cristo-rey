import Pagos from './../views/pagos/Index.vue';
const routes = [
	{
		path: '/Pagos',
		name: 'pagos',
		component: Pagos,
		meta: {
			name: 'Control de Pagos'
		}
	}
];
export default routes;