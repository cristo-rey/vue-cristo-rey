/* eslint-disable no-unused-vars */
import Vue from 'vue';
import VueRouter from 'vue-router';
import Pruebas from './../views/Pruebas.vue';
import Eliminados from './../views/Eliminados.vue';

import authRoutes from './auth';
import userRoutes from './users';
import rolesAndAbilitiesRoutes from './roles';
import pacientesRoutes from './pacientes';
import DoctoresRoutes from './doctores';
import EstudiosRoutes from './estudios';
import AnalisisRoutes from './analisis';
import OrdenesRoutes from './ordenes';
import PagosRoutes from './pagos';
import HelpRoutes from './help';
import RangosRoutes from './rangos';
import MuestrasRoutes from './muestras';

Vue.use(VueRouter);

const routes = [
	...authRoutes,
	...userRoutes,
	...rolesAndAbilitiesRoutes,
	...pacientesRoutes,
	...DoctoresRoutes,
	...EstudiosRoutes,
	...AnalisisRoutes,
	...OrdenesRoutes,
	...PagosRoutes,
	...HelpRoutes,
	...RangosRoutes,
	...MuestrasRoutes,
	{
		path: '/Pruebas',
		name: 'pruebas',
		component: Pruebas,
		meta: {
			name: 'Componente para pruebas'
		}
	},
	{
		path: '/Eliminados',
		name: 'eliminados',
		component: Eliminados,
		meta: {
			name: 'Registros Eliminados'
		}
	}
];

const router = new VueRouter({
	routes,
	linkExactActiveClass: 'nav-item active',
	mode: 'history'
});

router.beforeEach((to, from, next) => {
	window.scrollTo(0, 0);
	next();
});

export default router;
