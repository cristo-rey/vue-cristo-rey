import Rangos from './../views/rangos/Index.vue';
import Form from './../views/rangos/Form.vue';
const routes = [
	{
		path: '/Rangos',
		name: 'rangos',
		component: Rangos,
		meta: {
			name: 'Rangos de Edades para Referencias'
		}
	},
	{
		path: '/Rangos/nuevo',
		name: 'rangos.create',
		component: Form,
		meta: {
			name: 'Nuevo Rango de edades'
		}
	}
];
export default routes;
