import Usuarios from './../views/users/index.vue';
import UserForm from './../views/users/formulario.vue';
import UserAbilities from './../views/users/abilities.vue';
const routes = [
	{
		path: '/Usuarios/permisos/:id',
		name: 'user.abilities',
		component: UserAbilities,
		meta: {
			name: 'Permisos del Usuario'
		}
	},
	{
		path: '/Usuarios/editar/:id',
		name: 'user.edit',
		component: UserForm,
		meta: {
			name: 'Editando Usuario'
		}
	},
	{
		path: '/Usuarios/nuevo',
		name: 'user.create',
		component: UserForm,
		meta: {
			name: 'Nuevo Usuario'
		}
	},
	{
		path: '/Usuarios',
		name: 'users',
		component: Usuarios,
		meta: {
			name: 'Control de Usuarios'
		}
	}
];
export default routes;
