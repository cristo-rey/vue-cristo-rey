import Index from './../views/estudios/Index.vue';
import Form from './../views/estudios/Form.vue';
const routes = [
	{
		path: '/Estudios',
		name: 'estudios',
		component: Index,
		meta: {
			name: 'Catálogo de Estudios'
		}
	},
	{
		path: '/Estudios/nuevo',
		name: 'estudios.create',
		component: Form,
		meta: {
			name: 'Nuevo Estudio'
		}
	},
	{
		path: '/Estudios/editar/:id',
		name: 'estudios.edit',
		component: Form,
		meta: {
			name: 'Editar Estudio'
		}
	}
];
export default routes;
