import Index from './../views/muestras/Index.vue';
import Form from './../views/muestras/Form.vue';
import Details from './../views/muestras/Details.vue';

const routes = [
	{
		path: '/Muestras',
		name: 'muestras',
		component: Index,
		meta: {
			name: 'Control de Muestras'
		}
	},
	{
		path: '/Muestras/nueva',
		name: 'muestras.create',
		component: Form,
		meta: {
			name: 'Nueva Muestra'
		}
	},
	{
		path: '/Muestras/detalles/:id',
		name: 'muestras.details',
		component: Details,
		meta: {
			name: 'Flujo de la Muestra'
		}
	}
];
export default routes;
