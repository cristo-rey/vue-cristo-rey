import Index from './../views/ordenes/Index.vue';
import Show from './../views/ordenes/Show.vue';
const routes = [
	{
		path: '/Ordenes',
		name: 'ordenes',
		component: Index,
		meta: {
			name: 'Control de Ordenes'
		}
	},
	{
		path: '/Ordenes/detalles/:id',
		name: 'ordenes.show',
		component: Show,
		meta: {
			name: 'Detalle de Orden'
		}
	}
];
export default routes;
