import Index from './../views/help/Index.vue';
const routes = [
	{
		path: '/Soporte',
		name: 'help',
		component: Index,
		meta: {
			name: 'Tickets de Soporte'
		}
	}
];
export default routes;
