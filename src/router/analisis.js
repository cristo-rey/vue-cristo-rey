import Index from './../views/analisis/Index.vue';
import Form from './../views/analisis/Form.vue';
import Show from './../views/analisis/Show.vue';
const routes = [
	{
		path: '/Analisis',
		name: 'analisis',
		component: Index,
		meta: {
			name: 'Catálogo de Análisis'
		}
	},
	{
		path: '/Analisis/nuevo',
		name: 'analisis.create',
		component: Form,
		meta: {
			name: 'Nuevo Análisis'
		}
	},
	{
		path: '/Analisis/Detalles/:id',
		name: 'analisis.edit',
		component: Show,
		meta: {
			name: 'Detalles Análisis'
		}
	}
];
export default routes;
