import Abilities from './../views/abilities/index.vue';
import AbilityForm from './../views/abilities/abilityForm.vue';
import Roles from './../views/roles/index.vue';
import RoleForm from './../views/roles/form.vue';
const routes = [
	{
		path: '/Roles/editar/:id',
		name: 'role.edit',
		component: RoleForm,
		meta: {
			name: 'Editar Rol'
		}
	},
	{
		path: '/Roles/nuevo',
		name: 'role.create',
		component: RoleForm,
		meta: {
			name: 'Nuevo Rol'
		}
	},
	{
		path: '/Roles',
		name: 'roles',
		component: Roles,
		meta: {
			name: 'Control de Roles del Sistema'
		}
	},
	{
		path: '/Permisos/nuevo',
		name: 'ability.create',
		component: AbilityForm,
		meta: {
			name: 'Nuevo Permiso'
		}
	},
	{
		path: '/Permisos',
		name: 'abilities',
		component: Abilities,
		meta: {
			name: 'Control de Permisos'
		}
	}
];
export default routes;
