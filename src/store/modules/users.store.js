/* eslint-disable no-unused-vars */
import router from '../../router';
const url = 'users';
const initialState = () => ({
	headers: [],
	collection: [],
	resource: undefined,
	loader: false,
	abilities: [],
	roles: []
});
export default {
	namespaced: true,
	state: initialState(),
	mutations: {
		setInitialState(state) {
			const newState = initialState();
			Object.keys(newState).forEach(key => {
				state[key] = newState[key];
			});
		},
		setHeaders(state, payload) {
			state.headers = payload;
		},
		setCollection(state, payload) {
			state.collection = payload;
		},
		setResource(state, payload) {
			state.resource = payload;
		},
		setLoader(state, payload) {
			state.loader = payload;
		},
		setAbilities(state, payload) {
			state.abilities = payload;
		},
		setRoles(state, payload) {
			state.roles = payload;
		}
	},
	actions: {
		async getAll({ commit, dispatch }) {
			commit('setCollection', []);
			commit('setResource', undefined);
			commit('setErrors', [], { root: true });
			commit('setLoader', true);
			window.axios
				.get(url)
				.then(resp => {
					commit('setCollection', resp.data.collection);
					commit('setHeaders', resp.data.headers);
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader', false));
		},
		async getUser({ commit, dispatch }, id) {
			commit('setErrors', [], { root: true });
			window.axios
				.get(`${url}/${id}`)
				.then(resp => {
					commit('setResource', resp.data);
					router.push({ name: 'user.edit', params: { id } }).catch(() => {});
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader', false));
		},
		async storeUser({ commit, dispatch, state }) {
			commit('setErrors', [], { root: true });
			commit('setLoader', true);
			let user = state.resource;
			if (!user.id) {
				await window.axios
					.post(url, user)
					.then(resp => {
						window.swal(resp.data.message, { icon: 'success', timer: 1500 });
						router.push({ name: 'users' });
					})
					.catch(e => dispatch('errores', e, { root: true }))
					.finally(() => commit('setLoader', false));
			} else {
				await window.axios
					.put(`${url}/${user.id}`, user)
					.then(resp => {
						window.swal(resp.data.message, { icon: 'success', timer: 1500 });
						router.push({ name: 'users' });
					})
					.catch(e => dispatch('errores', e, { root: true }))
					.finally(() => commit('setLoader', false));
			}
		},
		async disableUser({ commit, dispatch }, id) {
			commit('setLoader', true);
			await window.axios
				.delete(`${url}/${id}`)
				.then(resp => {
					window.swal(resp.data.message, { icon: 'success', timer: 1500 });
					commit('setCollection', resp.data.collection);
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader', false));
		},
		async enableUser({ commit, dispatch }, id) {
			await window.axios
				.post(`${url}/restaurar/${id}`)
				.then(resp => {
					window.swal(resp.data.message, { icon: 'success', timer: 1500 });
					commit('setCollection', resp.data.collection);
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader', false));
		},
		async getAbilities({ commit, dispatch }, id) {
			commit('setLoader', true);
			await window.axios
				.get(`${url}/abilities/${id}`)
				.then(resp => {
					commit('setResource', resp.data.resource);
					commit('setAbilities', resp.data.abilities);
					commit('setRoles', resp.data.roles);
					router
						.push({ name: 'user.abilities', params: { id } })
						.catch(() => {});
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader', false));
		},
		async sendAbilities({ commit, dispatch, state }) {
			commit('setErrors', [], { root: true });
			commit('setLoader', true);
			let user = state.resource;
			await window.axios
				.post(`${url}/setAbilities/${user.id}`, { permisos: user.permisos })
				.then(resp => {
					commit('setResource', resp.data.resource);
					window.swal(resp.data.message, { icon: 'success', timer: 1500 });
					router.push({ name: 'users' }).catch(() => {});
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader', false));
		},
		async setRole({ commit, dispatch, state }) {
			commit('setErrors', [], { root: true });
			commit('setLoader', true);
			let user = state.resource,
				role = user.role ? user.role : '';
			await window.axios
				.post(`${url}/setRole/${user.id}`, { role: role })
				.then(resp => {
					commit('setResource', resp.data.resource);
					window.swal(resp.data.message, { icon: 'success', timer: 1500 });
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader', false));
		}
	}
};
