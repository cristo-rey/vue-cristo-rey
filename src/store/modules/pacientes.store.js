/* eslint-disable no-unused-vars */
import router from '../../router';
const url = 'pacientes';
const initialState = () => ({
	headers: [],
	collection: [],
	resource: undefined,
	loader: false,
	paginado: {
		currentPage: null,
		totalPaginas: null,
		registros: null,
		nombre: null,
		paterno: null,
		materno: null,
		perPage: 15
	},
	ordenes: undefined,
	doctores: undefined,
	estudios: undefined,
	analisis: undefined
});
export default {
	namespaced: true,
	state: initialState(),
	mutations: {
		setInitialState(state) {
			const newState = initialState();
			Object.keys(newState).forEach(key => {
				state[key] = newState[key];
			});
		},
		setHeaders(state, payload) {
			state.headers = payload;
		},
		setCollection(state, payload) {
			state.collection = payload;
		},
		setResource(
			state,
			payload = {
				nombre: '',
				paterno: '',
				materno: '',
				nacimiento: null,
				sexo: '',
				email: '',
				telefono: ''
			}
		) {
			state.resource = payload;
		},
		setLoader(state, payload = false) {
			state.loader = payload;
		},
		setOrdenes(state, payload) {
			state.ordenes = payload;
		},
		setDoctores(state, payload) {
			state.doctores = payload;
		},
		setEstudios(state, payload) {
			state.estudios = payload;
		},
		setAnalisis(state, payload) {
			state.analisis = payload;
		}
	},
	actions: {
		async getAll({ commit, dispatch }) {
			commit('setCollection', undefined);
			commit('setLoader', true);
			window.axios
				.get(url)
				.then(resp => {
					commit('setCollection', resp.data.collection);
					commit('setHeaders', resp.data.headers);
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		async getResource({ commit, dispatch }, item) {
			commit('setLoader', true);
			let id = item.id,
				nombre = `Editando al Paciente ${item.nombre}`;
			window.axios
				.get(`${url}/${id}`)
				.then(resp => {
					commit('setResource', resp.data);
					router
						.push({ name: 'pacientes.edit', params: { id, nombre } })
						.catch(() => {});
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		async sendResource({ commit, dispatch, state }) {
			commit('setLoader', true);
			commit('setErrors', [], { root: true });
			let paciente = state.resource;
			if (!paciente.id) {
				await window.axios
					.post(url, paciente)
					.then(resp => {
						window.swal(resp.data.message, { icon: 'success', timer: 1500 });
						dispatch('getPaginado');
						router.push({ name: 'pacientes' }).catch(() => {});
					})
					.catch(e => dispatch('errores', e, { root: true }))
					.finally(() => commit('setLoader'));
			} else {
				await window.axios
					.put(`${url}/${paciente.id}`, paciente)
					.then(resp => {
						window.swal(resp.data.message, { icon: 'success', timer: 1500 });
						dispatch('getPaginado');
						router.push({ name: 'pacientes' }).catch(() => {});
					})
					.catch(e => dispatch('errores', e, { root: true }))
					.finally(() => commit('setLoader'));
			}
		},
		async removePaciente({ commit, dispatch, state }, id) {
			commit('setLoader', true);
			await window.axios
				.delete(`${url}/${id}`)
				.then(resp => {
					window.swal(resp.data.message, { icon: 'success', timer: 1500 });
					dispatch('getPaginado');
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		async getPaginado({ state, commit, dispatch }) {
			commit('setLoader', true);
			commit('setCollection');
			state.paginado.totalPaginas = undefined;
			await window.axios
				.get(`${url}/paginado`, {
					params: {
						page: state.paginado.currentPage,
						nombre: state.paginado.nombre,
						paterno: state.paginado.paterno,
						materno: state.paginado.materno
					}
				})
				.then(resp => {
					commit('setCollection', resp.data.collection);
					commit('setHeaders', resp.data.headers);
					state.paginado.totalPaginas = resp.data.last_page;
					state.paginado.registros = resp.data.registros;
					state.paginado.currentPage = resp.data.current_page;
					if (state.paginado.totalPaginas < state.paginado.currentPage) {
						state.paginado.currentPage = 1;
					}
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		async historial({ commit, dispatch }, id) {
			commit('setLoader', true);
			await window.axios
				.get(`${url}/historial/${id}`)
				.then(resp => {
					commit('setOrdenes', resp.data.ordenes);
					commit('setResource', resp.data.paciente);
					let nombre = `Historial de ordenes ${resp.data.paciente.nombre}`;
					router
						.push({
							name: 'pacientes.historial',
							params: { id, nombre }
						})
						.catch(() => {});
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		async nuevaOrden({ commit, dispatch }, id) {
			commit('setLoader', true);
			await window.axios
				.get(`${url}/nuevaOrden/${id}`)
				.then(resp => {
					commit('setDoctores', resp.data.doctores);
					commit('setEstudios', resp.data.estudios);
					commit('setAnalisis',resp.data.analisis)
					commit('setResource', resp.data.paciente);
					let nombre = `Nueva Orden  ${resp.data.paciente.nombre}`;
					router
						.push({
							name: 'pacientes.nuevaOrden',
							params: { id, nombre }
						})
						.catch(() => {});
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		}
	}
};
