/* eslint-disable no-unused-vars */
import router from '../../router';
const url = 'tickets';
const initialState = () => ({
	loader: false,
	collection: undefined,
	paginado: {
		current_page: 1,
		last_page: null,
		registros: null,
		perPage: 15,
		status: null,
		buscada: null
	},
	resource: undefined,
	status: []
});
export default {
	namespaced: true,
	state: initialState(),
	mutations: {
		setInitialState(state) {
			const newState = initialState();
			Object.keys(newState).forEach(key => {
				state[key] = newState[key];
			});
		},
		setCollection(state, payload = []) {
			state.collection = payload;
		},
		setLoader(state, payload = false) {
			state.loader = payload;
		},
		setPaginado(state, payload) {
			state.paginado = payload;
		},
		setResource(state, payload = {}) {
			state.resource = payload;
		},
		setStatus(state, payload) {
			state.status = payload;
		}
	},
	actions: {
		async getAll({ commit, dispatch, state }) {
			commit('setCollection');
			commit('setLoader', true);
			let params = {
				page: state.paginado.currentPage > 1 ? state.paginado.currentPage : null
			};
			if (state.paginado.status) {
				params.status = state.paginado.status;
			}
			window.axios
				.get(url, { params })
				.then(resp => {
					let paginado = resp.data.paginado;
					paginado.buscada = null;
					commit('setCollection', resp.data.collection);
					if (state.paginado.currentPage > paginado.lastPage) {
						paginado.currentPage = 1;
					}
					commit('setPaginado', paginado);
					commit('setStatus', resp.data.statusAll);
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		}
	}
};
