/* eslint-disable no-unused-vars */
import router from '../../router';
const url = 'roles';
const initialState = () => ({
	headers: [],
	collection: [],
	resource: undefined,
	loader: false,
	abilities: []
});
export default {
	namespaced: true,
	state: initialState(),
	mutations: {
		setInitialState(state) {
			const newState = initialState();
			Object.keys(newState).forEach(key => {
				state[key] = newState[key];
			});
		},
		setHeaders(state, payload) {
			state.headers = payload;
		},
		setCollection(state, payload) {
			state.collection = payload;
		},
		setResource(state, payload) {
			state.resource = payload;
		},
		setLoader(state, payload) {
			state.loader = payload;
		},
		setAbilities(state, payload) {
			state.abilities = payload;
		}
	},
	actions: {
		async getAll({ commit, dispatch }) {
			commit('setCollection', []);
			commit('setResource', undefined);
			commit('setErrors', [], { root: true });
			commit('setLoader', true);
			window.axios
				.get(url)
				.then(resp => {
					commit('setCollection', resp.data.collection);
					commit('setHeaders', resp.data.headers);
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader', false));
		},
		nuevo({ commit }) {
			commit('setResource', {});
			router.push({ name: 'role.create' }).catch(() => {});
		},
		async sendRole({ commit, dispatch, state }) {
			commit('setErrors', [], { root: true });
			commit('setLoader', true);
			let role = state.resource;
			if (!role.id) {
				window.axios
					.post(url, role)
					.then(resp => {
						window.swal(resp.data.message, { icon: 'success', timer: 1500 });
						router.push({ name: 'roles' }).catch(() => {});
					})
					.catch(e => dispatch('errores', e, { root: true }))
					.finally(() => commit('setLoader', false));
			} else {
				window.axios
					.put(`${url}/${role.id}`, role)
					.then(resp => {
						window.swal(resp.data.message, { icon: 'success', timer: 1500 });
						router.push({ name: 'roles' }).catch(() => {});
					})
					.catch(e => dispatch('errores', e, { root: true }))
					.finally(() => commit('setLoader', false));
			}
		},
		async getRole({ commit, dispatch }, id) {
			commit('setErrors', [], { root: true });
			commit('setLoader', true);
			await window.axios
				.get(`${url}/${id}`)
				.then(resp => {
					commit('setAbilities', resp.data.abilities);
					commit('setResource', resp.data.resource);
					router.push({ name: 'role.edit', params: { id } }).catch(() => {});
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader', false));
		},
		async deleteRole({ commit, dispatch }, id) {
			commit('setLoader', true);
			window.axios
				.delete(`${url}/${id}`)
				.then(resp => {
					window.swal(resp.data.message, { icon: 'success', timer: 1500 });
					commit('setCollection', resp.data.collection);
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader', false));
		},
		async deleteAll({ commit, dispatch }) {
			commit('setLoader', true);
			window.axios
				.post(`${url}/deleteSinUso`)
				.then(resp => {
					window.swal(resp.data.message, { icon: 'success', timer: 1500 });
					commit('setCollection', resp.data.collection);
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader', false));
		}
	}
};
