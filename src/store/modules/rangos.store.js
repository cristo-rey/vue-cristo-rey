/* eslint-disable no-unused-vars */
import router from '../../router';
const url = 'rangoFechas';
const initialState = () => ({
	loader: false,
	collection: undefined,
	paginado: {
		current_page: 1,
		last_page: null,
		registros: null,
		perPage: 15,
		buscada: null
	},
	headers: [],
	categorias: undefined,
	resource: undefined,
	rangosAll: undefined
});
export default {
	namespaced: true,
	state: initialState(),
	mutations: {
		setInitialState(state) {
			const newState = initialState();
			Object.keys(newState).forEach(key => {
				state[key] = newState[key];
			});
		},
		setCollection(state, payload = []) {
			state.collection = payload;
		},
		setLoader(state, payload = false) {
			state.loader = payload;
		},
		setPaginado(state, payload) {
			state.paginado = payload;
		},
		setHeaders(state, payload) {
			state.headers = payload;
		},
		setResource(state, payload = {}) {
			state.resource = payload;
		},
		setRangosAll(state, payload) {
			state.rangosAll = payload;
		}
	},
	actions: {
		async getAll({ commit, dispatch, state }) {
			commit('setLoader', true);
			commit('setCollection');
			let page = state.paginado.buscada
				? state.paginado.buscada
				: state.paginado.currentPage;
			await window.axios
				.get(url, {
					params: {
						page: page
					}
				})
				.then(resp => {
					commit('setCollection', resp.data.collection);
					commit('setPaginado', resp.data.paginado);
					commit('setHeaders', resp.data.headers);
					commit('setRangosAll', resp.data.rangosAll);
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		nuevo({ commit }) {
			commit('setResource');
			router.push({ name: 'rangos.create' }).catch(() => {});
		},
		async sendResource({ commit, dispatch, state }) {
			let resource = state.resource;
			commit('setLoader', true);
			commit('setErrors', [], { root: true });
			if (!resource.id) {
				await window.axios
					.post(url, resource)
					.then(resp => {
						window.swal(resp.data.message, { icon: 'success', timer: 1500 });
						dispatch('getAll');
						router.push({ name: 'rangos' }).catch(() => {});
					})
					.catch(e => dispatch('errores', e, { root: true }))
					.finally(() => commit('setLoader'));
			}
		},
		async destroyResource({ commit, dispatch }, id) {
			commit('setLoader', true);
			await window.axios
				.delete(`${url}/${id}`)
				.then(resp => {
					window.swal(resp.data.message, { icon: 'success', timer: 1500 });
					dispatch('getAll');
					router.push({ name: 'rangos' }).catch(() => {});
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		}
	}
};
