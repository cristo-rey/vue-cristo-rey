/* eslint-disable no-unused-vars */
import router from '../../router';
const url = 'ordens';
const initialState = () => ({
	headers: [],
	collection: undefined,
	resource: undefined,
	loader: false,
	paginado: {
		currentPage: null,
		totalPaginas: null,
		registros: null,
		perPage: 15
	},
	paciente: undefined
});
export default {
	namespaced: true,
	state: initialState(),
	mutations: {
		setInitialState(state) {
			const newState = initialState();
			Object.keys(newState).forEach(key => {
				state[key] = newState[key];
			});
		},
		setHeaders(state, payload) {
			state.headers = payload;
		},
		setCollection(state, payload = []) {
			state.collection = payload;
		},
		setResource(state, payload = {}) {
			state.resource = payload;
		},
		setLoader(state, payload = false) {
			state.loader = payload;
		},
		setPaginado(state, payload) {
			state.paginado = payload;
		},
		setPaciente(state, payload) {
			state.paciente = payload;
		},
		setDoctor(state, payload) {
			state.doctor = payload;
		},
		setAnalisis(state, payload) {
			state.analisis = payload;
		},
		setPagos(state, payload) {
			state.pagos = payload;
		}
	},
	actions: {
		async getAll({ commit, dispatch, state }) {
			commit('setCollection');
			commit('setLoader', true);
			window.axios
				.get(url, {
					params: {
						page: state.paginado.currentPage
					}
				})
				.then(resp => {
					let paginado = resp.data.paginado;
					paginado.buscada = null;
					commit('setHeaders', resp.data.headers);
					commit('setCollection', resp.data.collection);
					if (state.paginado.currentPage > paginado.lastPage) {
						paginado.currentPage = 1;
					}
					commit('setPaginado', paginado);
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		async sendOrden({ commit, dispatch }, data) {
			commit('setLoader', true);
			commit('setErrors', [], { root: true });
			await window.axios
				.post(url, data)
				.then(resp => {
					dispatch('getAll');
					window.swal(resp.data.message, { icon: 'success', timer: 1500 });
					router.push({ name: 'ordenes' });
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		async showOrden({ commit, dispatch }, item) {
			commit('setLoader', true);
			commit('setErrors', [], { root: true });
			let id = item.id,
				nombre = `Detalles Orden ${item.folio}`;
			await window.axios
				.get(`${url}/${id}`)
				.then(resp => {
					commit('setResource', resp.data.resource);
					commit('setPaciente', resp.data.paciente);
					commit('setDoctor', resp.data.doctor);
					commit('setAnalisis', resp.data.analisis);
					commit('setPagos', resp.data.pagos);
					router
						.push({ name: 'ordenes.show', params: { id, nombre } })
						.catch(() => {});
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		async deleteOrden({ commit, dispatch }, id) {
			commit('SetLoader', true);
			await window.axios
				.delete(`${url}/${id}`)
				.then(resp => {
					window.swal(resp.data.message, { icon: 'success', timer: 1500 });
					dispatch('getAll');
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		}
	}
};
