/* eslint-disable no-unused-vars */
import router from '../../router';
const url = 'analisis';
const initialState = () => ({
	loader: false,
	collection: undefined,
	paginado: {
		current_page: 1,
		last_page: null,
		registros: null,
		perPage: 15,
		tipo: null,
		nombre: null,
		buscada: null,
		rangos: []
	},
	headers: [],
	tipos: undefined,
	resource: undefined,
	unidadesMedida: undefined,
	referencia: undefined
});
export default {
	namespaced: true,
	state: initialState(),
	mutations: {
		setInitialState(state) {
			const newState = initialState();
			Object.keys(newState).forEach(key => {
				state[key] = newState[key];
			});
		},
		setCollection(state, payload = []) {
			state.collection = payload;
		},
		setLoader(state, payload = false) {
			state.loader = payload;
		},
		setPaginado(state, payload) {
			state.paginado = payload;
		},
		setHeaders(state, payload) {
			state.headers = payload;
		},
		setTipos(state, payload = []) {
			state.tipos = payload;
		},
		setResource(state, payload = {}) {
			state.resource = payload;
		},
		setRangos(state, payload) {
			state.rangos = payload;
		},
		setUnidadesMedida(state, payload) {
			state.unidadesMedida = payload;
		},
		setReferencia(state, payload = {}) {
			state.referencia = payload;
		},
		setReferencias(state, payload) {
			state.resource.referencias = payload;
		}
	},
	actions: {
		async getAll({ commit, dispatch, state }) {
			commit('setCollection');
			commit('setLoader', true);
			commit('setTipos');
			let page = state.paginado.buscada
				? state.paginado.buscada
				: state.paginado.currentPage;
			window.axios
				.get(url, {
					params: {
						page: page,
						nombre: state.paginado.nombre,
						tipo: state.paginado.tipo
					}
				})
				.then(resp => {
					let paginado = resp.data.paginado;
					paginado.buscada = null;
					commit('setHeaders', resp.data.headers);
					commit('setCollection', resp.data.collection);
					commit('setTipos', resp.data.tipos);
					if (state.paginado.currentPage > paginado.lastPage) {
						paginado.currentPage = 1;
					}
					commit('setPaginado', paginado);
					commit('setRangos', resp.data.rangos);
					commit('setUnidadesMedida', resp.data.unidadesMedida);
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		nuevo({ commit }) {
			commit('setResource');
			router.push({ name: 'analisis.create' }).catch(() => {});
		},
		async getAnalisis({ commit, dispatch }, item) {
			let nombre = `Detalles análisis ${item.nombre}`,
				id = item.id;
			commit('setLoader', true);
			await window.axios
				.get(`${url}/${id}`)
				.then(resp => {
					commit('setResource', resp.data.resource);
					commit('setReferencias', resp.data.referencias);
					router
						.push({ name: 'analisis.edit', params: { id, nombre } })
						.catch(() => {});
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		async sendAnalisis({ commit, dispatch, state }) {
			commit('setLoader', true);
			commit('setErrors', [], { root: true });
			let analisis = state.resource;
			if (!analisis.id) {
				await window.axios
					.post(url, analisis)
					.then(resp => {						
						dispatch('getAll');
						dispatch('getAnalisis', resp.data.resource);
						window.swal(resp.data.message, { icon: 'success', timer: 1500 });
					})
					.catch(e => dispatch('errores', e, { root: true }))
					.finally(() => commit('setLoader'));
			} else {
				await window.axios
					.put(`${url}/${analisis.id}`, analisis)
					.then(resp => {						
						dispatch('getAll');
						dispatch('getAnalisis', analisis);
						window.swal(resp.data.message, { icon: 'success', timer: 1500 });
					})
					.catch(e => dispatch('errores', e, { root: true }))
					.finally(() => commit('setLoader'));
			}
		},
		async deleteAnalisis({ commit, dispatch }, id) {
			commit('setLoader', true);
			commit('setErrors', [], { root: true });
			await window.axios
				.delete(`${url}/${id}`)
				.then(resp => {					
					dispatch('getAll');
					window.swal(resp.data.message, { icon: 'success', timer: 1500 });
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		async sendReferencia({ commit, dispatch, state }) {
			let referencia = state.referencia;
			commit('setLoader', true);
			commit('setErrors', [], { root: true });
			if (!referencia.id) {
				window.axios
					.post('referencias', referencia)
					.then(resp => {						
						commit('setReferencias', resp.data.referencias);
						commit('setUnidadesMedida', resp.data.unidadesMedida);
						dispatch('getAnalisis', { id: state.resource.id });
						dispatch('getAll');
						window.swal(resp.data.message, { icon: 'success', timer: 1500 });
					})
					.catch(e => dispatch('errores', e, { root: true }))
					.finally(() => commit('setLoader'));
			} else {
				window.axios
					.put(`referencias/${referencia.id}`, referencia)
					.then(resp => {						
						commit('setReferencias', resp.data.referencias);
						commit('setUnidadesMedida', resp.data.unidadesMedida);
						dispatch('getAnalisis', { id: state.resource.id });
						dispatch('getAll');
						window.swal(resp.data.message, { icon: 'success', timer: 1500 });
					})
					.catch(e => dispatch('errores', e, { root: true }))
					.finally(() => commit('setLoader'));
			}
		},
		async removeReferencia({ commit, dispatch, state }, id) {
			commit('setLoader', true);
			commit('setErrors', [], { root: true });
			await window.axios
				.delete(`referencias/${id}`)
				.then(resp => {				
					commit('setReferencias', resp.data.referencias);
					commit('setUnidadesMedida', resp.data.unidadesMedida);
					dispatch('getAnalisis', { id: state.resource.id });
					dispatch('getAll');
					window.swal(resp.data.message, { icon: 'success', timer: 1500 });
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		}
	}
};
