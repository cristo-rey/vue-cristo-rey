/* eslint-disable no-unused-vars */
import router from '../../router';
const url = 'estudios';
const initialState = () => ({
	loader: false,
	collection: undefined,
	paginado: {
		current_page: 1,
		last_page: null,
		registros: null,
		perPage: 15,
		categoria: null,
		nombre: null,
		buscada: null
	},
	headers: [],
	categorias: undefined,
	resource: undefined,
	analisis: undefined
});
export default {
	namespaced: true,
	state: initialState(),
	mutations: {
		setInitialState(state) {
			const newState = initialState();
			Object.keys(newState).forEach(key => {
				state[key] = newState[key];
			});
		},
		setCollection(state, payload = []) {
			state.collection = payload;
		},
		setLoader(state, payload = false) {
			state.loader = payload;
		},
		setPaginado(state, payload) {
			state.paginado = payload;
		},
		setHeaders(state, payload) {
			state.headers = payload;
		},
		setCategorias(state, payload = []) {
			state.categorias = payload;
		},
		setResource(state, payload = {}) {
			state.resource = payload;
		},
		setAnalisis(state, payload) {
			state.analisis = payload;
		}
	},
	actions: {
		async getAll({ commit, dispatch, state }) {
			commit('setCollection');
			commit('setLoader', true);
			commit('setCategorias');
			window.axios
				.get(url, {
					params: {
						page: state.paginado.currentPage,
						nombre: state.paginado.nombre,
						categoria: state.paginado.categoria
					}
				})
				.then(resp => {
					let paginado = resp.data.paginado;
					paginado.buscada = null;
					commit('setHeaders', resp.data.headers);
					commit('setCollection', resp.data.collection);
					commit('setCategorias', resp.data.categorias);
					if (state.paginado.currentPage > paginado.lastPage) {
						paginado.currentPage = 1;
					}
					commit('setPaginado', paginado);
					commit('setAnalisis', resp.data.tests);
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		nuevo({ commit }) {
			commit('setResource', { resource: {} });
			router.push({ name: 'estudios.create' }).catch(() => {});
		},
		async getEstudio({ commit, dispatch }, item) {
			let nombre = `Editando estudio ${item.nombre}`,
				id = item.id;
			commit('setLoader', true);
			await window.axios
				.get(`${url}/${id}`)
				.then(resp => {
					commit('setResource', resp.data);
					router
						.push({ name: 'estudios.edit', params: { id, nombre } })
						.catch(() => {});
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		async sendEstudio({ commit, dispatch, state }) {
			commit('setLoader', true);
			commit('setErrors', [], { root: true });
			let estudio = state.resource.resource;
			if (!estudio.id) {
				await window.axios
					.post(url, estudio)
					.then(resp => {
						window.swal(resp.data.message, { icon: 'success', timer: 1500 });
						dispatch('getAll');
						commit('setResource', resp.data.resource);
						let nombre = `Editando estudio ${resp.data.resource.resource.nombre}`,
							id = resp.data.resource.resource.id;
						router
							.push({ name: 'estudios.edit', params: { id, nombre } })
							.catch(() => {});
					})
					.catch(e => dispatch('errores', e, { root: true }))
					.finally(() => commit('setLoader'));
			} else {
				await window.axios
					.put(`${url}/${estudio.id}`, estudio)
					.then(resp => {
						window.swal(resp.data.message, { icon: 'success', timer: 1500 });
						dispatch('getAll');
					})
					.catch(e => dispatch('errores', e, { root: true }))
					.finally(() => commit('setLoader'));
			}
		},
		async deleteEstudio({ commit, dispatch }, id) {
			commit('setLoader', true);
			commit('setErrors', [], { root: true });
			await window.axios
				.delete(`${url}/${id}`)
				.then(resp => {
					window.swal(resp.data.message, { icon: 'success', timer: 1500 });
					dispatch('getAll');
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		}
	}
};
