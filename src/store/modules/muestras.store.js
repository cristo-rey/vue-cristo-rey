/* eslint-disable no-unused-vars */
import router from '../../router';
const url = 'muestras';
const initialState = () => ({
	loader: false,
	collection: undefined,
	paginado: {
		currentPage: 1,
		lastPage: 1,
		registros: 0,
		perPage: 15,
		buscada: null
	},
	headers: [],
	categorias: undefined,
	resource: undefined,
	ordenes: undefined
});
export default {
	namespaced: true,
	state: initialState(),
	mutations: {
		setInitialState(state) {
			const newState = initialState();
			Object.keys(newState).forEach(key => {
				state[key] = newState[key];
			});
		},
		setCollection(state, payload = []) {
			state.collection = payload;
		},
		setLoader(state, payload = false) {
			state.loader = payload;
		},
		setPaginado(state, payload) {
			state.paginado = payload;
		},
		setHeaders(state, payload) {
			state.headers = payload;
		},
		setCategorias(state, payload = []) {
			state.categorias = payload;
		},
		setResource(state, payload = {}) {
			state.resource = payload;
		},
		setOrdenes(state, payload = undefined) {
			state.ordenes = payload;
		}
	},
	actions: {
		async getAll({ commit, dispatch, state }) {
			commit('setCollection');
			commit('setOrdenes');
			commit('setLoader', true);
			commit('setCategorias');
			window.axios
				.get(url, {
					params: {
						page: state.paginado.currentPage
					}
				})
				.then(resp => {
					let paginado = resp.data.paginado;
					paginado.buscada = null;
					commit('setHeaders', resp.data.headers);
					commit('setCollection', resp.data.collection);
					if (state.paginado.currentPage > paginado.lastPage) {
						paginado.currentPage = 1;
					}
					commit('setPaginado', paginado);
					commit('setOrdenes', resp.data.ordenes);
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		nueva({ commit }) {
			commit('setResource');
			router.push({ name: 'muestras.create' }).catch(() => {});
		},
		async storeResource({ commit, dispatch, state }) {
			commit('setErrors', [], { root: true });
			commit('setLoader', true);
			let item = state.resource;
			if (!item.id) {
				await window.axios
					.post(url, item)
					.then(resp => {
						dispatch('getAll');
						router.push({ name: 'muestras' }).catch(() => {});
						window.swal(resp.data.message, { icon: 'success', timer: 1500 });
					})
					.catch(e => dispatch('errores', e, { root: true }))
					.finally(() => commit('setLoader'));
			}
		},
		async destroyResource({ commit, dispatch }, id) {
			commit('setErrors', [], { root: true });
			commit('setLoader', true);
			await window.axios
				.delete(`${url}/${id}`)
				.then(resp => {
					dispatch('getAll');
					window.swal(resp.data.message, { icon: 'success', timer: 1500 });
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		async detailMuestra({ commit, dispatch }, item) {
			commit('setLoader', true);
			let nombre = `Detalle de la Muestra ${item.folio}`,
				id = item.id;
			await window.axios
				.get(`${url}/${id}`)
				.then(resp => {
					commit('setResource', resp.data);
					router
						.push({ name: 'muestras.details', params: { id, nombre } })
						.catch(() => {});
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		async recibirLab({ commit, dispatch }, id) {
			commit('setLoader', true);
			await window.axios
				.put(`${url}/recibirLab/${id}`)
				.then(resp => {
					dispatch('getAll');
					window.swal (resp.data.message,{icon:'success',timer:1500});
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		}
	}
};
