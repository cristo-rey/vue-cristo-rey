/* eslint-disable no-unused-vars */
import router from '../../router';
const url = 'doctors';
const initialState = () => ({
	headers: [],
	collection: [],
	resource: undefined,
	loader: false,
	paginado: {
		currentPage: null,
		totalPaginas: null,
		registros: null,
		nombre: null,
		paterno: null,
		materno: null,
		perPage: 15
	}
});
export default {
	namespaced: true,
	state: initialState(),
	mutations: {
		setInitialState(state) {
			const newState = initialState();
			Object.keys(newState).forEach(key => {
				state[key] = newState[key];
			});
		},
		setHeaders(state, payload) {
			state.headers = payload;
		},
		setCollection(state, payload = []) {
			state.collection = payload;
		},
		setResource(
			state,
			payload = {
				nombre: '',
				paterno: '',
				materno: '',
				nacimiento: null,
				sexo: '',
				email: '',
				telefono: ''
			}
		) {
			state.resource = payload;
		},
		setLoader(state, payload = false) {
			state.loader = payload;
		}
	},
	actions: {
		async getAll({ commit, dispatch }) {
			commit('setCollection', undefined);
			commit('setLoader', true);
			window.axios
				.get(url)
				.then(resp => {
					commit('setCollection', resp.data.collection);
					commit('setHeaders', resp.data.headers);
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		async getResource({ commit, dispatch }, item) {
			commit('setLoader', true);
			let id = item.id,
				nombre = `Editando al Doctor ${item.nombre}`;
			window.axios
				.get(`${url}/${id}`)
				.then(resp => {
					commit('setResource', resp.data);
					router
						.push({ name: 'doctores.edit', params: { id, nombre } })
						.catch(() => {});
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		async sendResource({ commit, dispatch, state }) {
			commit('setLoader', true);
			commit('setErrors', [], { root: true });
			let doctor = state.resource;
			if (!doctor.id) {
				await window.axios
					.post(url, doctor)
					.then(resp => {
						window.swal(resp.data.message, { icon: 'success', timer: 1500 });
						dispatch('getPaginado');
						router.push({ name: 'doctores' }).catch(() => {});
					})
					.catch(e => dispatch('errores', e, { root: true }))
					.finally(() => commit('setLoader'));
			} else {
				await window.axios
					.put(`${url}/${doctor.id}`, doctor)
					.then(resp => {
						window.swal(resp.data.message, { icon: 'success', timer: 1500 });
						dispatch('getPaginado');
						router.push({ name: 'doctores' }).catch(() => {});
					})
					.catch(e => dispatch('errores', e, { root: true }))
					.finally(() => commit('setLoader'));
			}
		},
		async removeDoctor({ commit, dispatch, state }, id) {
			commit('setLoader', true);
			await window.axios
				.delete(`${url}/${id}`)
				.then(resp => {
					window.swal(resp.data.message, { icon: 'success', timer: 1500 });
					dispatch('getPaginado');
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		async getPaginado({ state, commit, dispatch }) {
			commit('setLoader', true);
			commit('setCollection');
			state.paginado.totalPaginas = undefined;
			await window.axios
				.get(`${url}/paginado`, {
					params: {
						page: state.paginado.currentPage,
						nombre: state.paginado.nombre,
						paterno: state.paginado.paterno,
						materno: state.paginado.materno
					}
				})
				.then(resp => {
					commit('setCollection', resp.data.collection);
					commit('setHeaders', resp.data.headers);
					state.paginado.totalPaginas = resp.data.last_page;
					state.paginado.registros = resp.data.registros;
					state.paginado.currentPage = resp.data.current_page;
					if (state.paginado.totalPaginas < state.paginado.currentPage) {
						state.paginado.currentPage = 1;
					}
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		}
	}
};
