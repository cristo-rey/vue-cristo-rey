/* eslint-disable no-unused-vars */
import router from '../../router';
const initialState = () => ({
	collection: undefined,
	paginado: { currentPage: 1 },
	loader: false,
	headers: [],
	tipos: ['Pagos', 'Doctores', 'Pacientes', 'Ordenes', 'Muestras']
});
export default {
	namespaced: true,
	state: initialState(),
	mutations: {
		setInitialState(state) {
			const newState = initialState();
			Object.keys(newState).forEach(key => {
				state[key] = newState[key];
			});
		},
		setCollection(state, payload = []) {
			state.collection = payload;
		},
		setLoader(state, payload = false) {
			state.loader = payload;
		},
		setPaginado(state, payload = undefined) {
			state.paginado = payload;
		},
		setHeaders(state, payload = []) {
			state.headers = payload;
		}
	},
	actions: {
		async getRegistros({ commit, dispatch, state }, url) {
			commit('setLoader', true);
			commit('setCollection');
			commit('setHeaders');
			window.axios
				.get(url, {
					params: {
						page: state.paginado.currentPage
					}
				})
				.then(resp => {
					commit('setCollection', resp.data.collection);
					commit('setPaginado', resp.data.paginado);
					commit('setHeaders', resp.data.headers);
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		}
	}
};
