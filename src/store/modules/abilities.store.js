/* eslint-disable no-unused-vars */
import router from '../../router';
const url = 'abilities';
const initialState = () => ({
	headers: [],
	collection: [],
	resource: {},
	loader: false,
	search: undefined
});
export default {
	namespaced: true,
	state: initialState(),
	mutations: {
		setInitialState(state) {
			const newState = initialState();
			Object.keys(newState).forEach(key => {
				state[key] = newState[key];
			});
		},
		setHeaders(state, payload) {
			state.headers = payload;
		},
		setCollection(state, payload) {
			state.collection = payload;
		},
		setResource(state, payload) {
			state.resource = payload;
		},
		setLoader(state, payload) {
			state.loader = payload;
		},
		setSearch(state, payload) {
			state.search = payload;
		}
	},
	actions: {
		async getAll({ commit, dispatch }) {
			commit('setCollection', []);
			commit('setErrors', [], { root: true });
			commit('setLoader', false);
			window.axios
				.get(url)
				.then(resp => {
					commit('setCollection', resp.data.collection);
					commit('setHeaders', resp.data.headers);
				})
				.catch(e => dispatch('errores', e, { root: true }));
		},
		async newAbility({ commit }) {
			commit('setResource', {});
			commit('setLoader', false);
			router.push({ name: 'ability.create' });
		},
		async sendAbility({ commit, dispatch, state }) {
			const ability = state.resource;
			if (!ability.id) {
				await window.axios
					.post(url, ability)
					.then(resp => {
						window.swal(resp.data.message, { icon: 'success', timer: 1500 });
						commit('setCollection', resp.data.collection);
						router.push({ name: 'abilities' });
					})
					.catch(e => dispatch('errores', e, { root: true }))
					.finally(() => commit('setLoader', false));
			} else {
				//
			}
		}
	}
};
