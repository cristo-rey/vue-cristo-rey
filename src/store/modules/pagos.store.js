/* eslint-disable no-unused-vars */
import router from '../../router';
const url = 'pagos';
const initialState = () => ({
	loader: false,
	collection: undefined,
	paginado: {
		current_page: 1,
		last_page: null,
		registros: null,
		perPage: 15,
		categoria: null,
		nombre: null,
		buscada: null
	},
	headers: [],
	resource: undefined
});
export default {
	namespaced: true,
	state: initialState(),
	mutations: {
		setInitialState(state) {
			const newState = initialState();
			Object.keys(newState).forEach(key => {
				state[key] = newState[key];
			});
		},
		setCollection(state, payload = []) {
			state.collection = payload;
		},
		setLoader(state, payload = false) {
			state.loader = payload;
		},
		setPaginado(state, payload) {
			state.paginado = payload;
		},
		setHeaders(state, payload) {
			state.headers = payload;
		},
		setResource(state, payload = {}) {
			state.resource = payload;
		}
	},
	actions: {
		async getAll({ commit, dispatch, state }) {
			commit('setCollection');
			commit('setLoader', true);
			window.axios
				.get(url, {
					params: {
						page: state.paginado.currentPage
					}
				})
				.then(resp => {
					let paginado = resp.data.paginado;
					paginado.buscada = null;
					commit('setHeaders', resp.data.headers);
					commit('setCollection', resp.data.collection);
					if (state.paginado.currentPage > paginado.lastPage) {
						paginado.currentPage = 1;
					}
					commit('setPaginado', paginado);
					commit('setResource');
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		},
		async destroyPago({ commit, dispatch }, id) {
			commit('setLoader', true);
			window.axios
				.delete(`${url}/${id}`)
				.then(resp => {
					window.swal(resp.data.message, { icon: 'success', timer: 1500 });
					dispatch('getAll');
				})
				.catch(e => dispatch('errores', e, { root: true }))
				.finally(() => commit('setLoader'));
		}
	}
};
